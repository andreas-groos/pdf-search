import React, { useEffect } from "react";
import Snippet from "./Snippet";

const SearchResultsBox = ({
  searchResults = {},
  searchTerm,
  handleSnippetClick,
  filteredDocument
}) => {
  useEffect(() => {}, [filteredDocument]);
  return (
    <section id="search-box-results" className="section ">
      {Object.keys(searchResults).map(r => {
        if (!filteredDocument || filteredDocument === r) {
          return (
            <div key={r}>
              <span className="tag is-info is-large m-t-md w-100">{r}</span>
              {searchResults[r].map((o, i) => (
                <Snippet
                  key={i}
                  snippet={o}
                  handleSnippetClick={handleSnippetClick}
                />
              ))}
            </div>
          );
        } else return null;
      })}
    </section>
  );
};

export default SearchResultsBox;
