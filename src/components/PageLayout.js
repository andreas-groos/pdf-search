import React from "react";

const PageLayout = ({ children }) => {
  return (
    <div className="page">
      <section className="section">
        <div className="container is-fluid">{children}</div>
      </section>
    </div>
  );
};

export default PageLayout;
