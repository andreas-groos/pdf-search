import React from "react";
import SearchInfoList from "./SearchInfoList";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

const Searchbox = ({
  handleInputChange,
  handleSearchSubmit,
  searchInfo,
  filteredDocument,
  setFilteredDocument,
  loading
}) => {
  const onInputChange = e => {
    handleInputChange({ name: e.target.name, value: e.target.value });
  };
  return (
    <div id="search-box">
      <div className="field">
        <label className="label">All of the words</label>
        <div className="control">
          <form onSubmit={handleSearchSubmit("allwords")}>
            <input
              name="allwords"
              className="input"
              type="text"
              onChange={onInputChange}
              placeholder="search term + enter"
            />
          </form>
        </div>
      </div>
      <div className="field m-t-md">
        <label className="label">Any of the words</label>
        <div className="control">
          <form data-form="allwords" onSubmit={handleSearchSubmit("anywords")}>
            <input
              name="anywords"
              className="input"
              type="text"
              onChange={onInputChange}
              placeholder="search term + enter"
            />
          </form>
        </div>
        <div className="field m-t-md">
          <label className="label">Exact Search Phrase</label>
          <div className="control">
            <form onSubmit={handleSearchSubmit("term")}>
              <input
                name="term"
                className="input"
                type="text"
                onChange={onInputChange}
                placeholder="search term + enter"
              />
            </form>
          </div>
        </div>
      </div>
      {loading && (
        <div className="m-t-lg m-b-lg has-text-centered">
          <FontAwesomeIcon icon={faSpinner} size="4x" spin />
        </div>
      )}
      {searchInfo && (
        <div>
          <p className="has-text-grey is-size-7">{`${
            searchInfo.total
          } results (${searchInfo.took} ms)`}</p>
          <SearchInfoList
            info={searchInfo}
            filteredDocument={filteredDocument}
            setFilteredDocument={setFilteredDocument}
          />
        </div>
      )}
    </div>
  );
};

export default Searchbox;
