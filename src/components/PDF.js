import React, { useEffect, useState } from "react";
import { Document, Page } from "react-pdf";
import { pdfjs } from "react-pdf";
import "react-pdf/dist/Page/AnnotationLayer.css";
import { SERVER_URL, SERVER_PORT } from "../constants";

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${
  pdfjs.version
}/pdf.worker.js`;

const highlightPattern = (text, pattern) => {
  const splitText = text.split(new RegExp(pattern, "ig"));
  if (splitText.length <= 1) {
    return text;
  }

  const matches = text.match(new RegExp(pattern, "ig"));

  return (
    splitText &&
    splitText.reduce(
      (arr, element, index) =>
        matches[index]
          ? [...arr, element, <mark>{matches[index]}</mark>]
          : [...arr, element],
      []
    )
  );
};

const PDF = ({ currentPage, searchTerm, page }) => {
  const [pdfURL, setPdfURL] = useState();
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    const url =
      process.env.NODE_ENV === "development"
        ? `http://localhost:${SERVER_PORT}`
        : `${SERVER_URL}:${SERVER_PORT}`;
    setLoading(true);
    if (!page || page === currentPage.location) {
      let count = currentPage.location.toString().padStart(4, "0");

      setPdfURL(`${url}/split_pdf/${currentPage.filename}_${count}.pdf`);
    } else {
      let count = page.toString().padStart(4, "0");
      setPdfURL(`${url}/split_pdf/${currentPage.filename}_${count}.pdf`);
    }
  }, [currentPage, page]);

  const makeTextRenderer = searchText => textItem =>
    highlightPattern(textItem.str, searchText);

  if (!pdfURL) {
    return null;
  }
  return (
    <div id="pdf">
      <Document file={pdfURL} loading="">
        <Page
          customTextRenderer={makeTextRenderer(searchTerm)}
          pageNumber={1}
          onLoadSuccess={() => setLoading(false)}
        />
      </Document>
      {/* {loading && <div className="skeleton" />} */}
    </div>
  );
};

export default PDF;
