import React, { useEffect, useState } from "react";
import Searchbox from "./Searchbox";
import SearchResultsBox from "./SearchResultsBox";
import debounce from "lodash/debounce";
import axios from "axios";
import { SERVER_PORT, SERVER_URL } from "../constants";

import { getExtendedSearchInfo, sortSearchResults } from "../utils";

const initialFormValues = {
  searchTerm: "",
  exact: true
};

const Sidebar = ({ setCurrentPage, handleFormValuesChange }) => {
  const [formValues, setFormValues] = useState(initialFormValues);
  const [searchResults, setSearchResults] = useState();
  const [searchInfo, setSearchInfo] = useState();
  const [filteredDocument, setFilteredDocument] = useState(null);
  const [loading, setLoading] = useState(false);

  const fetchResults = async (searchTerm, type) => {
    setLoading(true);
    setSearchResults();
    setSearchInfo();
    setFilteredDocument(null);
    const url =
      process.env.NODE_ENV === "development"
        ? `http://localhost:${SERVER_PORT}`
        : `${SERVER_URL}:${SERVER_PORT}`;
    var t0 = performance.now();

    try {
      let res = await axios.get(`${url}/search`, {
        params: { term: searchTerm, type }
      });
      var t1 = performance.now();
      const info = getExtendedSearchInfo(res.data.result.hits);
      setSearchInfo({
        ...info,
        took: (t1 - t0).toFixed(0)
      });

      const sortedSearchResults = sortSearchResults(
        res.data.result.hits.hits,
        info.titles,
        type
      );
      setLoading(false);
      setSearchResults(sortedSearchResults);
    } catch (err) {
      setLoading(false);
      console.log("err", err);
    }
  };

  const handleSearchSubmit = type => e => {
    e.preventDefault();
    fetchResults(formValues[type], type);
  };

  const handleInputChange = ({ name, value }) => {
    setFormValues({ ...formValues, [name]: value });
    handleFormValuesChange({ ...formValues, [name]: value });
  };

  const handleSnippetClick = page => {
    setCurrentPage(page);
  };

  return (
    <div id="sidebar">
      <div className="columns sidebar-wrapper">
        <div className="column left-side">
          <Searchbox
            handleInputChange={handleInputChange}
            handleSearchSubmit={handleSearchSubmit}
            searchInfo={searchInfo}
            filteredDocument={filteredDocument}
            setFilteredDocument={setFilteredDocument}
            loading={loading}
          />
        </div>
        {searchResults && (
          <div className="column right-side">
            <SearchResultsBox
              searchResults={searchResults}
              searchTerm={formValues.searchTerm}
              handleSnippetClick={handleSnippetClick}
              filteredDocument={filteredDocument}
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default Sidebar;
