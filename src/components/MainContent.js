import React, { useState, useEffect } from "react";
import PDF from "./PDF";
import { Document, Page } from "react-pdf";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowCircleLeft,
  faArrowCircleRight
} from "@fortawesome/free-solid-svg-icons";

const MainContent = ({ currentPage, searchTerm }) => {
  const [page, setPage] = useState();
  useEffect(() => {
    currentPage && setPage(currentPage.location);
  }, [currentPage]);
  const pageBackward = () => {
    if (page) {
      setPage(page - 1);
    } else {
      const { location } = currentPage && currentPage._source;
      setPage(location - 1);
    }
  };
  const pageForward = () => {
    if (page) {
      setPage(page + 1);
    } else {
      const { location } = currentPage && currentPage._source;
      setPage(location + 1);
    }
  };
  // console.log("currentPage", currentPage);
  // console.log("page", page);
  if (!currentPage) return null;
  return (
    <div id="main-content">
      <div className="top-wrapper">
        <FontAwesomeIcon
          onClick={pageBackward}
          icon={faArrowCircleLeft}
          size="4x"
        />
        <div>
          <h3 className="is-size-3">{currentPage.title}</h3>
          <h5 className="is-size-5">by {currentPage.author}</h5>
          {/* <h6 className="is-size-6">Summary: {currentPage._source.summary}</h6> */}
          {/* <p>{currentPage._source.location}</p> */}
          <p className="has-text-centered is-size-5">Page: {page}</p>
        </div>
        <FontAwesomeIcon
          onClick={pageForward}
          icon={faArrowCircleRight}
          size="4x"
        />
      </div>
      <PDF currentPage={currentPage} searchTerm={searchTerm} page={page} />
    </div>
  );
};

export default MainContent;
