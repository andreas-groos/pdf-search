import React from "react";

const Snippet = ({ snippet, handleSnippetClick }) => {
  const s = snippet.highlight.text;
  return (
    <div className="snippet" onClick={() => handleSnippetClick(snippet)}>
      <div className="card">
        <header className="card-header">
          <div className="level">
            <p className="card-header-title">Page {snippet.location}</p>
          </div>
        </header>
        <div className="card-content">
          <div className="content">
            {s.map((ss, i) => {
              return (
                <p key={`snip_${i}`} dangerouslySetInnerHTML={{ __html: ss }} />
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Snippet;
