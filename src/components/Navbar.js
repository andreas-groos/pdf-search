import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

const Navbar = ({ documents }) => {
  return (
    <section id="top-nav">
      <div className="container is-fluid">
        <nav className="navbar" role="navigation" aria-label="main navigation">
          <div className="navbar-brand">
            <a className="navbar-item" href="https://bulma.io">
              <img
                src="https://bulma.io/images/bulma-logo.png"
                alt="navbar-logo"
                width="112"
                height="28"
              />
            </a>
          </div>
          <div id="navbarBasicExample" className="navbar-menu">
            <div className="navbar-start">
              <Link className="navbar-item" to="/">
                Home
              </Link>

              <Link className="navbar-item" to="/documents">
                All Documents
              </Link>

              <Link className="navbar-item" to="/about">
                About
              </Link>

              {documents && (
                <div className="navbar-item has-dropdown is-hoverable">
                  <a className="navbar-link">Document</a>

                  <div className="navbar-dropdown">
                    {documents.map(d => {
                      return (
                        <Link
                          key={d.filename}
                          to={`/document/${d.filename}`}
                          className="navbar-item"
                        >
                          {d.title}
                        </Link>
                      );
                    })}
                  </div>
                </div>
              )}
            </div>
          </div>
        </nav>
      </div>
    </section>
  );
};

export default Navbar;
