import React from "react";

const SearchInfoList = ({ info, filteredDocument, setFilteredDocument }) => {
  return (
    <div id="search-info-list">
      {info.titles && (
        <div>
          <hr />
          <p>
            <b>Appears in the following titles:</b>
          </p>
          {info.titles.map((t, i) => (
            <span
              key={i}
              onClick={() => setFilteredDocument(t)}
              className={
                filteredDocument && filteredDocument !== t
                  ? "tag is-medium m-t-md w-100 is-light"
                  : "tag is-info is-medium m-t-md w-100"
              }
            >
              {t}
            </span>
          ))}
          {filteredDocument && (
            <span
              onClick={() => setFilteredDocument(null)}
              className="tag is-medium is-primary m-t-md w-100"
            >
              Show in all documents
            </span>
          )}
        </div>
      )}
    </div>
  );
};

export default SearchInfoList;
