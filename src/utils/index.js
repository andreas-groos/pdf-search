import uniq from "lodash/uniq";
import sortBy from "lodash/sortBy";
import { CLIENT_RENEG_LIMIT } from "tls";

export const getExtendedSearchInfo = res => {
  let { hits, total } = res;
  const authors = uniq(hits.map(h => h._source.author));
  const titles = uniq(hits.map(h => h._source.title));
  const files = uniq(hits.map(h => h._source.filename + ".pdf"));
  return { authors, files, titles, total };
};

export const sortSearchResults = (hits, titles, searchType) => {
  let h = hits.map(h => ({ ...h._source, highlight: h.highlight }));
  let keyed = {};
  h.forEach(i => {
    if (!keyed[i.title]) {
      keyed[i.title] = [];
    }
    keyed[i.title].push(i);
  });
  Object.entries(keyed).forEach(([key, value]) => {
    keyed[key] = sortBy(keyed[key], o => o.location);
  });
  return keyed;
};
