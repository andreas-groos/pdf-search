import React, { useEffect, useState } from "react";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import Home from "./pages/Home";
import NotFound from "./pages/NotFound";
import About from "./pages/About";
import Documents from "./pages/Documents";
import SingleDocument from "./pages/SingleDocument";
import Navbar from "./components/Navbar";
import PageLayout from "./components/PageLayout";
import axios from "axios";

import { SERVER_PORT, SERVER_URL } from "./constants";
import "./style/style.scss";

function App() {
  const [documents, setDocuments] = useState([]);
  useEffect(() => {
    const url =
      process.env.NODE_ENV === "development"
        ? `http://localhost:${SERVER_PORT}`
        : `${SERVER_URL}:${SERVER_PORT}`;
    axios
      .get(`${url}/documents`)
      .then(res => {
        setDocuments(res.data.documents);
      })
      .catch(err => {
        console.log("err", err);
      });
  }, []);
  return (
    <div id="app">
      <Router>
        <div id="page-wrapper">
          <Navbar documents={documents} />
          <PageLayout>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route
                path="/documents"
                component={() => <Documents documents={documents} />}
              />
              <Route
                path="/document/:filename"
                component={props => (
                  <SingleDocument
                    location={props.location}
                    documents={documents}
                  />
                )}
              />
              <Route path="/about" component={About} />
              <Route component={NotFound} />
            </Switch>
          </PageLayout>
        </div>
      </Router>
    </div>
  );
}

export default App;
