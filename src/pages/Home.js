import React, { useState } from "react";
import Sidebar from "../components/Sidebar";
import MainContent from "../components/MainContent";

const Home = () => {
  const [currentPage, setCurrentPage] = useState();
  const [formValues, setFormValues] = useState({});
  const { searchTerm } = formValues;
  return (
    <div id="home-page">
      <div className="columns">
        <div className=" ">
          <Sidebar
            setCurrentPage={setCurrentPage}
            handleFormValuesChange={setFormValues}
          />
        </div>
        <div className="">
          <MainContent
            currentPage={currentPage}
            searchTerm={formValues.searchTerm}
          />
        </div>
      </div>
    </div>
  );
};

export default Home;
