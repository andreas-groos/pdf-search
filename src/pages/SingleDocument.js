import React, { useEffect, useState } from "react";
import axios from "axios";
import { Document, Page } from "react-pdf";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowCircleLeft,
  faArrowCircleRight
} from "@fortawesome/free-solid-svg-icons";
import { pdfjs } from "react-pdf";
import { SERVER_PORT, SERVER_URL } from "../constants";
import get from "lodash/get";

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${
  pdfjs.version
}/pdf.worker.js`;

const SingleDocument = props => {
  const [loadingStatus, setLoadingStatus] = useState(true);
  const [page, setPage] = useState(1);
  const [documentInfo, setDocumentInfo] = useState();
  const [pageInput, setPageInput] = useState(1);
  const filename = get(props, "location.pathname", "").replace(
    "/document/",
    ""
  );
  useEffect(() => {
    setLoadingStatus(true);
    let d = props.documents.filter(d => d.filename === filename);
    d && setDocumentInfo(d[0]);
    setPage(1);
  }, [filename, props.documents]);
  useEffect(() => {
    const checkKeyEvent = e => {
      if (e.key === "ArrowRight") {
        pageForward();
      }
      if (e.key === "ArrowLeft") {
        pageForward();
      }
    };
    window.addEventListener("keydown", checkKeyEvent);
    return function cleanup() {
      window.removeEventListener("keydown", checkKeyEvent);
    };
  });
  const pdfURL = (name, page) => {
    const url =
      process.env.NODE_ENV === "development"
        ? `http://localhost:${SERVER_PORT}`
        : `${SERVER_URL}:${SERVER_PORT}`;
    let count = page.toString().padStart(4, "0");
    const pdf =
      url +
      "/split_pdf/" +
      name.substring(0, name.length - 4) +
      "_" +
      count +
      ".pdf";
    return pdf;
  };
  const pageBackward = () => {
    if (page > 1) {
      setPage(page - 1);
    }
  };
  const pageForward = () => {
    if (page < documentInfo.pages - 1) {
      setPage(page + 1);
    }
  };
  const setPageNumber = e => {
    e.preventDefault();
    if (pageInput > 1 && pageInput <= documentInfo.pages) {
      setPage(parseInt(pageInput));
    }
  };
  return (
    <div id="single-document-page">
      <h3 className="is-size-3 has-text-centered">{filename}</h3>
      <p className="has-text-centered has-text-grey-light">
        <small>You can use the arrow keys to go forward/backward</small>
      </p>
      <br />
      {documentInfo && (
        <div className="columns is-centered">
          <div className="column is-3">
            <div className="field is-horizontal">
              <div className="field-label is-normal">
                <label className="label">Page:</label>
              </div>
              <div className="control">
                <form onSubmit={setPageNumber}>
                  <input
                    className="input is-info"
                    type="number"
                    placeholder={`1-${documentInfo.pages - 1}, hit enter`}
                    onChange={e => {
                      setPageInput(e.target.value);
                    }}
                  />
                </form>
              </div>
            </div>
          </div>
        </div>
      )}
      <div className="pdf-wrapper">
        <div className="left">
          <FontAwesomeIcon
            className="show-hover"
            onClick={pageBackward}
            icon={faArrowCircleLeft}
            size="4x"
          />
        </div>
        <div className="">
          <Document file={pdfURL(filename, page)} loading="">
            <Page
              onLoadSuccess={() => setLoadingStatus(false)}
              pageNumber={1}
            />
          </Document>
          {loadingStatus && <div className="skeleton" />}
        </div>
        <div className="right">
          <FontAwesomeIcon
            className="show-hover"
            onClick={pageForward}
            icon={faArrowCircleRight}
            size="4x"
          />
        </div>
      </div>
    </div>
  );
};

export default SingleDocument;
