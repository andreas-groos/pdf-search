import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { Document, Page } from "react-pdf";
import { pdfjs } from "react-pdf";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDownload } from "@fortawesome/free-solid-svg-icons";
import { SERVER_PORT, SERVER_URL } from "../constants";
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${
  pdfjs.version
}/pdf.worker.js`;

const truncateString = (str, num) => {
  if (!str) return;
  if (num <= 3) {
    return str.slice(0, num) + "…";
  }
  if (num >= str.length) {
    return str;
  }
  return str.slice(0, num - 3) + "…";
};

const Documents = ({ documents }) => {
  const [loadingStatus, setLoadingStatus] = useState([]);
  useEffect(() => {
    let arr = new Array(documents);
    arr.fill(false, 0, documents);
    setLoadingStatus(arr);
  }, [documents]);
  const pdfURL = name => {
    const url =
      process.env.NODE_ENV === "development"
        ? `http://localhost:${SERVER_PORT}`
        : `${SERVER_URL}:${SERVER_PORT}`;
    const pdf =
      url + "/split_pdf/" + name.substring(0, name.length - 4) + "_0001.pdf";
    return pdf;
  };

  const onDocumentLoadSuccess = i => {
    let arr = [...loadingStatus];
    arr[i] = true;
    setLoadingStatus(arr);
  };

  return (
    <div id="documents-page">
      <section className="section">
        <h1 className="is-size-1 has-text-centered">Documents</h1>
      </section>
      <div className="masonry">
        {documents.map((d, i) => {
          return (
            <Link key={i} to={`/document/${d.filename}`}>
              <div className="item">
                <div className="card ">
                  <div className="card-image">
                    <Document file={pdfURL(d.filename)} loading="">
                      <Page
                        onLoadSuccess={() => onDocumentLoadSuccess(i)}
                        pageNumber={1}
                      />
                    </Document>
                    {!loadingStatus[i] && <div className="skeleton" />}
                  </div>
                  <div className="card-header ">
                    <p className="card-header-title has-text-info  ">
                      {d.title}
                    </p>
                  </div>
                  <div className="card-content">
                    <p className="subtitle is-6 ">
                      <b>Author: </b>
                      {d.author}
                    </p>
                    {d.summary && (
                      <>
                        <p>
                          <b>Summary: </b>
                          {truncateString(d.summary, 70)}
                        </p>
                        <br />
                      </>
                    )}
                    <p>
                      <b>Pages: </b>
                      {d.pages}
                    </p>
                  </div>
                  <div className="card-footer">
                    <div className="card-footer-item">
                      {d.url === "No download URL specified" ? (
                        <p className="has-text-danger">No download link</p>
                      ) : (
                        <span>
                          <a href={d.url} target="_blank">
                            Download file
                          </a>
                          <FontAwesomeIcon
                            style={{ marginLeft: "0.5rem" }}
                            icon={faDownload}
                            color="hsl(217, 71%, 53%)"
                          />
                        </span>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </Link>
          );
        })}
      </div>
    </div>
  );
};

export default Documents;
